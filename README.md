<!-- MIT Expat License

Copyright 2021 Marek Sapota

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. -->

# Readable Time Parser

This Kotlin library can parse human readable strings into
`kotlin.time.Duration`.

## Examples

```kotlin
import org.sapota.marek.readable_time_parser.DurationParser

DurationParser.parse("12") // 12s
DurationParser.parse("12s")
DurationParser.parse("12h")
DurationParser.parse("12h 5m")
DurationParser.parse("8d 32h 18m 5s")
DurationParser.parse("2 hours")
DurationParser.parse("2 hours 5 minutes")
DurationParser.parse("3:12") // 3m 12s
DurationParser.parse("8:3:12") // 8h 3m 12s
```
