// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.readable_time_parser

import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class DurationParser private constructor() {
    private fun parseGroupTime(
        match: MatchResult,
        groupName: String,
        unit: DurationUnit,
    ): Duration? =
        try {
            match.groups[groupName]
        } catch (_: IllegalArgumentException) {
            null
        }
            ?.value?.toInt()?.toDuration(unit)

    private fun parseSeconds(match: MatchResult): Duration? =
        parseGroupTime(
            match,
            "seconds",
            DurationUnit.SECONDS,
        )

    private fun parseMinutes(match: MatchResult): Duration? =
        parseGroupTime(
            match,
            "minutes",
            DurationUnit.MINUTES,
        )

    private fun parseHours(match: MatchResult): Duration? =
        parseGroupTime(
            match,
            "hours",
            DurationUnit.HOURS,
        )

    private fun parseDays(match: MatchResult): Duration? =
        parseGroupTime(
            match,
            "days",
            DurationUnit.DAYS,
        )

    private fun parseTime(pattern: String, durationString: String): Duration? =
        Regex(pattern).let { regex ->
            regex.matchEntire(durationString)
        }?.let { match ->
            listOf(
                parseSeconds(match),
                parseMinutes(match),
                parseHours(match),
                parseDays(match),
            ).filter { it != null }.map { it!! }
        }?.let { durations ->
            if (durations.isEmpty()) {
                null
            } else {
                durations.reduce { a, b -> a + b }
            }
        }

    fun parseString(durationString: String): Duration? {
        val secondsPattern = "(?<seconds>[0-9]+)\\s*(s|sec|seconds)"
        val minutesPattern = "(?<minutes>[0-9]+)\\s*(m|min|minutes)"
        val hoursPattern = "(?<hours>[0-9]+)\\s*(h|hr|hours)"
        val daysPattern = "(?<days>[0-9]+)\\s*(d|days)"
        return parseTime("(?<seconds>[0-9]+)", durationString)
            // 1d 2h 3m 4s
            ?: parseTime(
                listOf(
                    daysPattern,
                    hoursPattern,
                    minutesPattern,
                    secondsPattern,
                )
                    .joinToString("\\s*") { "($it)?" },
                durationString,
            )
            // 01:02:03
            ?: parseTime(
                "(((?<hours>[0-9]+):)?(?<minutes>[0-9]+):)?(?<seconds>[0-9]+)",
                durationString,
            )
    }

    fun parse(durationString: String): Duration? =
        parseString(durationString.trim())

    companion object {
        private val parserSingleton = DurationParser()

        fun parse(durationString: String): Duration? =
            parserSingleton.parse(durationString)
    }
}
