// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.sapota.marek.readable_time_parser.DurationParser
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

data class ParsingTestCase(val humanReadable: String, val result: Duration?)

class DurationParserTest {
    val parsingTests = listOf(
        ParsingTestCase("123", 123.toDuration(DurationUnit.SECONDS)),
        ParsingTestCase("123s", 123.toDuration(DurationUnit.SECONDS)),
        ParsingTestCase("123 s", 123.toDuration(DurationUnit.SECONDS)),
        ParsingTestCase("123 sec", 123.toDuration(DurationUnit.SECONDS)),
        ParsingTestCase("123 seconds", 123.toDuration(DurationUnit.SECONDS)),
        ParsingTestCase(
            "   123    seconds    ",
            123.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12m 3s",
            12.toDuration(DurationUnit.MINUTES) +
                3.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "182m 3s",
            182.toDuration(DurationUnit.MINUTES) +
                3.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12h 3s",
            12.toDuration(DurationUnit.HOURS) +
                3.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12  h 3seconds",
            12.toDuration(DurationUnit.HOURS) +
                3.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12  hours 3  seconds",
            12.toDuration(DurationUnit.HOURS) +
                3.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12d 3h 45s",
            12.toDuration(DurationUnit.DAYS) +
                3.toDuration(DurationUnit.HOURS) +
                45.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12:3:45",
            12.toDuration(DurationUnit.HOURS) +
                3.toDuration(DurationUnit.MINUTES) +
                45.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "12:03:45",
            12.toDuration(DurationUnit.HOURS) +
                3.toDuration(DurationUnit.MINUTES) +
                45.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "47:83:95",
            47.toDuration(DurationUnit.HOURS) +
                83.toDuration(DurationUnit.MINUTES) +
                95.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "1234:4567:890",
            1234.toDuration(DurationUnit.HOURS) +
                4567.toDuration(DurationUnit.MINUTES) +
                890.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "03:45",
            3.toDuration(DurationUnit.MINUTES) +
                45.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "13:45",
            13.toDuration(DurationUnit.MINUTES) +
                45.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase(
            "13:5",
            13.toDuration(DurationUnit.MINUTES) +
                5.toDuration(DurationUnit.SECONDS),
        ),
        ParsingTestCase("", null),
        ParsingTestCase("nan", null),
        ParsingTestCase("10 foo", null),
        ParsingTestCase("10:8s", null),
        ParsingTestCase("10h:8", null),
    )

    @TestFactory
    fun testParsing() = parsingTests.map({ (humanReadable, result) ->
        DynamicTest.dynamicTest(
            "Test '$humanReadable' parses to '$result'",
        ) {
            Assertions.assertEquals(result, DurationParser.parse(humanReadable))
        }
    })
}
